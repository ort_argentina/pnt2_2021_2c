import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import ComponenteUno from './components/ComponenteUno';

export default class App extends React.Component {

  constructor(props) {
    super(props)
    
    this.state = {
      total: null,
      leyenda: ''
    }
    
    this.textHandler = this.textHandler.bind(this)
  }

  textHandler(params) {
    console.log(params.target.value)

    this.setState({
      leyenda: params.target.value
    })
  }

  render () {
    return (
      <View style={styles.container}>
        <Text>Ahora en modo Class</Text>
        <input id="texto" type="text" onKeyUp={this.textHandler}></input>
        <h4>{ this.state.leyenda }</h4>
        <br></br>
        <ComponenteUno x="2"></ComponenteUno>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
