import React, { Component } from 'react';

class ComponenteUno extends Component {

    constructor(props) {
        super(props);
        this.state = {
          valor: 0
        }
        
        this.textHandler = this.textHandler.bind(this)
    }
  
    textHandler(params) {
      this.setState({
        leyenda: params.target.value
      })
    }
    
    componentDidMount() {
      console.log('Montado')
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
      if (this.state.valor < 10)
        this.setState({
          valor: 10
        })
    }

    render() {
      return (
        <div>
          <h2>Componente Dos: { this.props.x } </h2>
          <input id="texto" type="number" onKeyUp={this.textHandler}></input>
        </div>
      );
    }
}

export default ComponenteUno;
