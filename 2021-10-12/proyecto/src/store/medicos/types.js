const AGREGARESPECIALIDAD = 'agregarEspecialidad'
const EDITARSPECIALIDAD = 'editarEspecialidad'
const ELIMINARESPECIALIDAD = 'eliminarEspecialidad'

const AGREGARMEDICO = 'agregarMedico'

export {
  AGREGARESPECIALIDAD,
  EDITARSPECIALIDAD,
  ELIMINARESPECIALIDAD,
  AGREGARMEDICO
}
