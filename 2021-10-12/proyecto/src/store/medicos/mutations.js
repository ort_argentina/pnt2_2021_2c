import { AGREGARESPECIALIDAD, AGREGARMEDICO } from './types'

export default {
  [AGREGARESPECIALIDAD] (state, data) {
    state.especialidades.push(data)
  },
  [AGREGARMEDICO] (state, data) {
    state.medicos[data.id] = data.datos
  }
}
