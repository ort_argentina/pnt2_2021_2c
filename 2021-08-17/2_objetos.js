class Gato {
  constructor(nombre) {
    console.log("Construyendo gatito")
    this.nombre = nombre
    this.peso = 100
  }

  maullar() {
    console.log("Miauuuuu, " + this.nombre);
  }
}

let g = new Gato('Pipo')
g.maullar()
console.log(g)

let s1 = JSON.stringify(g)
console.log(s1)

let o1 = JSON.parse(s1)
console.log(o1)

let o2 = Object.setPrototypeOf(o1, Gato.prototype)
console.log(o2)
o2.maullar()

//////////////////////////

let g2 = {
  nombre: 'Que soy',
  peso: 200,
  edad: 5,
  saltar: function () {
    console.log('Salto alto')
  },
  maullar: function () {
    console.log('maullo que soy')
  }
}

let o3 = Object.setPrototypeOf(g2, Gato.prototype)
console.log(o3)
o3.maullar()
