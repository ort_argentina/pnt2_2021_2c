const url = 'https://swapi.dev/api/people/1';

console.log('Pidiendo...');

// async-await
async function consultar() {
  console.log('Paso 2')
  let response = await fetch(url)
  console.log('Paso 3')
  let datos = await response.json()
  return datos
}

async function realizarConsulta() {
  console.log('paso 1')
  let r = await consultar()
  console.log(r)
  console.log('paso 4')
}

realizarConsulta().t

console.log('Fin programa')