const url = 'https://swapi.dev/api/people/1';

let xhttp = new XMLHttpRequest();

xhttp.onreadystatechange = () => {
  console.log(xhttp.readyState);
  if (xhttp.readyState == 4) {
    console.log(xhttp.status);
    if (xhttp.status == 200) {
      console.log(xhttp.responseText);
    }
  }
};

xhttp.open('GET', url);
xhttp.send();

/*
1- No enviado
2- Abierto y conectado
3- Loading
4- Done / Finalizado
*/
