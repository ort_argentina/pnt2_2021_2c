# pnt2_2021_2c

## URL Repositorio
  https://gitlab.com/ort_argentina/pnt2_2021_2c

## URL Grupo Telegram (Comunicacion no formal/oficial)
  https://t.me/pnt2ort  

## Clases grabadas
  https://drive.google.com/drive/folders/1AN85udq2_5dGBpjW8c7aZj2c8WuxXwpQ?usp=sharing


## Metodologia de evaluacion

  - 1 examenes parciales: Multiplechoice, completar, correspondencia
    En Noviembre

  - Asistencia.

  - 1 Trabajos practicos entrega en el final:
    - 2 ABM
    - 1 Pantalla Operatoria
    - 1 Pantalla de informe



# Temas
  - Javascript/JS6
  - Git
  - Vue.js
      - Quasar    --> App de celular
  - React.Native  --> App de celular

  Adicionales:
  - Docker
  - Cloud
  - Websocket


# Herramientas
  - Visual Studio Code
  - Git
  - Node.JS

