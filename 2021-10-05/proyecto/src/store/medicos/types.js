const AGREGARESPECIALIDAD = 'agregarEspecialidad'
const EDITARSPECIALIDAD = 'editarEspecialidad'
const ELIMINARESPECIALIDAD = 'eliminarEspecialidad'

export {
  AGREGARESPECIALIDAD,
  EDITARSPECIALIDAD,
  ELIMINARESPECIALIDAD
}
