import { AGREGARESPECIALIDAD } from './types'

export default {
  [AGREGARESPECIALIDAD] (state, data) {
    state.especialidades.push(data)
  }
}
