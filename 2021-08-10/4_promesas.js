// Promesas: Promise - Asincronismo
// JS Monothread

// Callback

// El constructor de la promesa, recibe una funcion
// la cual recibe 2 funciones
let prom1 = new Promise((resolve, reject) => {
  // Aca va la tarea de la promesa
  setTimeout(() => {
    console.log('Se finaliza la tarea');
    resolve('Ok');
  }, 3000);
});

prom1.then(mensaje => {
  console.log(mensaje);
});

// Solicitar un recurso a una web (API)
const url = 'https://swapi.dev/api/people/1';

console.log('Pidiendo...');
fetch(url)
  .then(response => {
    console.log(response);
    return response.json();
  })
  .then(data => {
    console.log(data);
  })
  .catch(err => {
    console.log(err);
  });

