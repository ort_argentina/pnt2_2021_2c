console.log('Hola');  // Comentario de linea

/*
 * Comentario 
 * multilinea
 */

// Nomenclatura: camelCase

/*
variables
funciones
objetos
*/

const pi = 3.14;

var x = 1;
let y = 1;

var x = "a";
console.log(x);

// let y = "B";

let a = [1,2,3,"abc","xyz"]   // Tratar de evitar, mezclar tipos
console.log(a)

// transpilacion TypeScript -> JS   (Babel)

function funcion1() {
  console.log("En la funcion")
}

funcion1()

let funcion2 = function() {
  console.log("Otra funcion")
}

funcion2()
console.log(funcion2.toString())

let funcion3 = funcion1
funcion3()

console.log(typeof funcion3)
console.log(typeof x)




// Arrow function  (Scope)
let funcion4 = (i,j) => {
  console.log(i+j)
}

funcion4(10,5)
