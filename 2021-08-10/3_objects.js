class Gato {
  constructor(nombre) {
    console.log("Construyendo gatito")
    this.nombre = nombre
    this.peso = 100
  }

  maullar() {
    console.log("Miauuuuu, " + this.nombre);
  }
}

let a = new Gato("Pepe")
a.maullar()
console.log(a.peso)

let b = new Gato("Pipo")
b.maullar()
b.color = "Negro"

console.log(b)