
// Forma 1 de crear un objeto

let x = new Object()
x.nombre = "Gabriel"
x.saludar = function() {
  console.log("Hola")
}

console.log(x)
x.saludar()

let x1 = JSON.stringify(x)
console.log(x1)

let x2 = JSON.parse(x1)
console.log(x2)

// Forma 2 de crear un objeto

let y = {
  nombre: "Gabriel",
  direccion: {
    calle: "Yatay",
    altura: 350
  },
  saludar: function() {
    console.log("Hola")
  }
}

console.log(y)